//Hallo Herr Zickermann, Ich konnte leider nicht durch eclipse hochladen, 
//deshalb erstelle ich  meine Arbeit in Bitbucket. Hoffentlich kommt kein Problem.
//�brigens habe ich festgestellt dass ich Methoden nicht richtig verstanden habe :( 
//LG Fulya 


import java.util.Scanner;

class test_fahrkarten
{
    private static Scanner tastatur = new Scanner(System.in);
    private static double fahrkartenbestellungErfassen() {
    	//Anzahl und Kostet der Tickets
        //---------------
    	int ticketzahl;
    	double ticketkostet;
    	double zuZahlenderBetrag;
    	
        System.out.println("Wie viele Tickets m�chten Sie kaufen?");
 	   	ticketzahl = tastatur.nextInt();
        System.out.println("Wie viele Ticketpreis m�chten Sie haben?");
        ticketkostet = tastatur.nextDouble();
        zuZahlenderBetrag = ticketzahl * ticketkostet;
        System.out.print("Zu zahlender Betrag (EURO): ");
        return zuZahlenderBetrag;
    }
    
    private static double fahrkartenBezahlen(double zuZahlen) {
    	// Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.println("Noch zu zahlen: " + (zuZahlen - eingezahlterGesamtbetrag) + " Euro");
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
    	return zuZahlen - eingezahlterGesamtbetrag;
    }
    	
    private static void fahrkartenAusgeben() {
    	 // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
    	
    public static void main(String[] args)
    {
       double zuZahlenderBetrag; 
       double r�ckgabebetrag;
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.println("Der R�ckgabebetrag in Hohe von " + r�ckgabebetrag + " EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}