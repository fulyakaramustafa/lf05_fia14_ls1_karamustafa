import java.util.Scanner;

public class �bung1 {
	
	public static void main(String[] args) {
	
		Scanner myScanner = new Scanner(System.in); 
		System.out.println("Bitte geben Sie eine ganze Zahl ein: ");
		int i = myScanner.nextInt();
		System.out.println("Sie haben " + i + " eingegeben!");
		
		System.out.println("Bitte geben Sie eine Zahl mit Nachkommastellen ein: ");
		float f = myScanner.nextFloat();
		System.out.println("Sie haben " + f + " eingegeben!");
		
		System.out.println("Bitte geben Sie einen beliebigen Name ein: ");
		String s = myScanner.next();
		System.out.println("Sie haben '" + s + "' eingegeben!");
	
		System.out.println("Bitte geben Sie einen Wahrheitswert ein (true / false): ");
		boolean b = myScanner.nextBoolean();
		System.out.println("Sie haben '" + b + "' eingegeben!");
		
		System.out.println("Bitte geben Sie einen einzelnen Buchstaben ein: ");
		char c = myScanner.next().charAt(0);
		System.out.println("Sie haben '" + c + "' eingegeben!");
		System.out.println("Herzlichen Gl�ckwunsch, das wars! ");
		
		
	}

}
