//Schreibe Sie ein Programm, welches zwei von Ihnen erdachte S�tze nacheinander in der 
//Konsole ausgibt. Es sollen dabei 2 Ausgabebefehle genutzt werden.

public class Aufgabe_1 {
	
	public static void main(String[] args) {
		
		String name = "Fulya";
		String sprache = "Java";
		
		System.out.println(" \" Hallo!\" ");
		System.out.println("Ich bin " + name + " und ich lerne " + sprache + "!"); 
		
		int xx = 2; 
		
		System.out.println("Heute ist unser " + xx + " . Unterricht." ); 
		
		String a = "*************" ;
		
		System.out.printf( "%10.1s\n" , a );
		System.out.printf( "%9.1s %.1s \n" , a, a );
		System.out.printf( "%9.2s %.2s \n" , a, a );
		System.out.printf( "%9.3s %.3s \n" , a, a );
		System.out.printf( "%9.4s %.4s \n" , a, a );
		System.out.printf( "%9.5s %.5s \n" , a, a );
		System.out.printf( "%11.3s\n" , a );
		System.out.printf( "%11.3s\n" , a );
		System.out.printf( "%11.3s\n" , a );
		
		
		
		System.out.printf( "%20.1s\n" , a );
		System.out.printf( "%21.3s\n" , a );
		System.out.printf( "%22.5s\n" , a );
		System.out.printf( "%23.7s\n" , a );
		System.out.printf( "%24.9s\n" , a );
		System.out.printf( "%25.11s\n" , a );
		System.out.printf( "%26.13s\n" , a );
		System.out.printf( "%21.3s\n" , a );
		System.out.printf( "%21.3s\n" , a );
		System.out.printf( "%21.3s\n" , a );
		
		double d = 22.4234234;
		System.out.printf( "%.2f\n" , d);
		
		double dd = 111.2222;
		System.out.printf( "%.2f\n" , dd);
		
		double ddd = 4.0;
		System.out.printf( "%.2f\n" , ddd);
		
		double aa = 1000000.551;
		System.out.printf( "%.2f\n" , aa);
		
		double bb = 97.34;
		System.out.printf( "%.2f\n" , bb);
		
		
	}
	
	
}