
public class Konfiguration {

	public static void main(String[] args) {
		
		String typ; 
		typ = "Automat AVR";
		String bezeichnung; 
		bezeichnung = "Q2021_FAB_A";
		String name; 
		name = typ + " " + bezeichnung; 
		System.out.println("Name: " + name);
		
		char sprachModul = 'd';
		System.out.println("Sprache: " + sprachModul);
		
		final byte PRUEFNR = 4;
		System.out.println("Pruefnummer : " + PRUEFNR);
		
		double maximum = 100.00;
		double patrone = 46.24; 
		double prozent = maximum - patrone;
		System.out.println("Fuellstand Patrone: " + prozent + " %");
		
		int muenzenEuro; 
		muenzenEuro = 130;
		int muenzenCent;
		muenzenCent = 1280;
		
		int summe;
		summe = muenzenCent + muenzenEuro * 100;
		
		int euro;
		euro = summe / 100; 
		System.out.println("Summe Euro: " + euro +  " Euro");
		
		
		int cent;
		cent = summe % 100;
		System.out.println("Summe Rest: " + cent +  " Cent");
		
		boolean status = (euro <= 150) && (euro >= 50) && (cent != 0) && (prozent >= 50.00) && (sprachModul == 'd') &&  (!(PRUEFNR == 5 || PRUEFNR == 6));
		System.out.println("Status: " + status);
				
		
	}
}
