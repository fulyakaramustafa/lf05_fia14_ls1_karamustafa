import java.util.Scanner;
public class uebung_A3_2 {
		
		public static void main(String[] args){
			 Scanner tastatur = new Scanner(System.in);
			 int zahl1;
			 int zahl2;
			 int ergebnis;
			 
			 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
			 zahl1 = tastatur.nextInt();
			 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
			 zahl2 = tastatur.nextInt();

			 ergebnis = addiere(zahl1, zahl2);
			 
			 System.out.print("Das Ergebnis der Addition lautet: \n");
			 System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);

			 int zahl3 = 5;
			 int zahl4 = 6; 
			 
			 ergebnis = addiere(zahl3, zahl4);
			 System.out.println(zahl3 + " + " + zahl4 + " = " + ergebnis);
		}
		
		 public static int addiere(int a, int b){
			 int summe = a + b;
			 return summe;
		

		 }

}