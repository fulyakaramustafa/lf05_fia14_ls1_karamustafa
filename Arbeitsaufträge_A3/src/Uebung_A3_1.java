//mittelwert

public class Uebung_A3_1 {

	public static void main(String[] args) {
		
		double x = 2.0;
		double y = 4.0; 
		double m; 
		
		m = berechneMittelwert(x, y); 
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
		
	}
	
	static double berechneMittelwert(double r1, double r2) {
		
		double m = (r1 + r2) / 2.0;
		return m;
	}		
}
